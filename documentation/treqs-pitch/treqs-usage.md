# TReqs useage

## Install 

- Clone the gitlab repository to local computer
- Set up a virtual python environment
- Open terminal, go to the folder that contains the gitlab treqs-ng

```
knauss$ source treqs_env/bin/activate
```

- With the virtual environment in place, install treqs

```
knauss$ cd treqs-ng
knauss$ pip install -e .
```

## Run

- With installation in place, we can now run `treqs` on the commandline.

```
knauss$ treqs --help
Usage: treqs [OPTIONS] COMMAND [ARGS]...

Options:
  --help  Show this message and exit.

Commands:
  check    Checks for consistency of treqs elements.
  create   Creates a treqs element and prints it on the command line.
  list     List treqs elements in this folder
  process  Process a treqs-controlled file, i.e.
```

## treqs list

- On the command line, in a folder that contains treqs-controlled requirements 

```
knauss$ treqs list --help
Usage: treqs list [OPTIONS]

  List treqs elements in this folder

Options:
  --type TEXT       Limit action to specified treqs element type
  --recursive TEXT  List treqs elements recursively in all subfolders.
  --filename TEXT   Give a file or directory to list from.
  --help            Show this message and exit.
```

## treqs list (example)

- Let's try it with the treqs requirements

```
knauss$ cd requirements
| Element type | Label | UID |
| :--- | :--- | :--- |
| requirement | ### 2.0 Parameters and default output of treqs list | a0820e06-9614-11ea-bb37-0242ac130002 |
| requirement | ### 2.1 Filter by type  | 437f09c6-9613-11ea-bb37-0242ac130002 |
| requirement | ### 2.2 Filter by ID  | a0820b4a-9614-11ea-bb37-0242ac130002 |
| requirement | ### 2.3 List all elements in a file | bc89e02a76c811ebb811cf2f044815f7 |
| requirement | ### 2.4 List treqs elements in a directory | 638fa22e76c911ebb811cf2f044815f7 |
```

## treqs list (example)

- If we layout the table as markdown, it is much more readable.

| Element type | Label | UID |
| :--- | :--- | :--- |
| requirement | ### 2.0 Parameters and default output of treqs list | a0820e06-9614-11ea-bb37-0242ac130002 |
| requirement | ### 2.1 Filter by type  | 437f09c6-9613-11ea-bb37-0242ac130002 |
| requirement | ### 2.2 Filter by ID  | a0820b4a-9614-11ea-bb37-0242ac130002 |
| requirement | ### 2.3 List all elements in a file | bc89e02a76c811ebb811cf2f044815f7 |
| requirement | ### 2.4 List treqs elements in a directory | 638fa22e76c911ebb811cf2f044815f7 |

- This allows us to get the ID for a treqs element, e.g. for tracelinks

## treqs create

```
knauss$ treqs create
Which type should the element have? [requirement]: unit-test
Which label should the element have? [my requirement]: ## arabic2roman test
<treqs-element id="54ed41e2867111eb91e5c4b301c00591" type="unit-test">

## arabic2roman test
add additional information/details here
</treqs-element>
```


## treqs check

```
knauss$ treqs check --help
Usage: treqs check [OPTIONS]

  Checks for consistency of treqs elements.

Options:
  --help  Show this message and exit.
```

## treqs process

::: columns

:::: {.column width=60%}
```
knauss$ treqs process --help
Usage: treqs process [OPTIONS] FILENAME

  Process a treqs-controlled file, i.e. generate content in protected areas.

Options:
  --recursive BOOLEAN         Process all subfolder recursively.  [default:
                              True]

  --plantuml / --no-plantuml  Toggle whether plantuml blocks should be
                              processed.  [default: True]

  --help                      Show this message and exit.
```

::::

:::: {.column width=40%}

```
@startuml process_plantuml
start
:Generate png files for each plantuml block;
:Create in-line markdown reference to png file;
if (in-line markdown reference exists) then (no)
  :add in-line markdown reference
after plantuml block;
endif
stop
@enduml
```
![process_plantuml](process_plantuml.png)

::::

:::
