from setuptools import setup

setup(
    name='treqs',
    version='0.4 (consolidated)',
    py_modules=['treqs'],
    install_requires=[
        'Click',
    ],
    entry_points='''
        [console_scripts]
        treqs=main:treqs
    ''',
)
