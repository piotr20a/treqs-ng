## Active developers

- Eric Knauss @oerich (Owner, Maintainer)
- @grischa_l (Maintainer)
- @rashidahk (Maintainer)

## Past contributions

TReqs has been developed over a period of time and many have contributed directly or indirectly. 

- [T-Reqs v1](https://github.com/regot-chalmers/treqs)
- [T-Reqs: Key idea and User Stories](https://arxiv.org/abs/1805.02769)

### Thesis work

- [Gebremichael, Mebrahtom Guesh: Master's thesis](https://odr.chalmers.se/handle/20.500.12380/300667), [gitlab](https://gitlab.com/mebrahtom/treqs)
- [Kasauli, Rashidah: PhD thesis](https://research.chalmers.se/en/publication/517099)
- [Alsahhar, Yazan and Bulai, Gabriel-Marian: Bachelor's thesis](https://gupea.ub.gu.se/handle/2077/62550)
- Chen, Yu-Jhen: Project work



