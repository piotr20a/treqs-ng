import unittest

from list_elements import list_elements

class TestListElements(unittest.TestCase):

    # <treqs-element id="38e94278a22f11eba9dca7925d1c5fe9" type="unittest">
    # Basic listing.
    # <treqs-link type="tests" target="a0820e06-9614-11ea-bb37-0242ac130002" />
    # <treqs-link type="tests" target="63ef8bfa76ae11ebb811cf2f044815f7" />
    # <treqs-link type="tests" target="bc89e02a76c811ebb811cf2f044815f7" />
    # </treqs-element>
    def test_list_elements(self):
        with self.assertLogs('treqs-on-git.treqs-ng', level='INFO') as captured:
            with self.assertRaises(SystemExit) as cm:
                le = list_elements()
                le.list_elements(None, './requirements/2-list-reqts.md', 'false', None, False, False)

        self.assertEqual(cm.exception.code, 0)

        self.assertEqual(len(captured.records), 14)
        self.assertEqual(captured.records[0].getMessage(), "file_traverser created")
        self.assertEqual(captured.records[1].getMessage(), "treqs_element_fatory created")
        self.assertEqual(captured.records[2].getMessage(), "list_elements created")
        self.assertEqual(captured.records[3].getMessage(), "Calling XML traversal with filename ./requirements/2-list-reqts.md")
        self.assertEqual(captured.records[4].getMessage(), "\n\n ### Processing elements in File ./requirements/2-list-reqts.md")
        self.assertEqual(captured.records[5].getMessage(), "| requirement | ### 2.0 Parameters and default output of treqs list | a0820e06-9614-11ea-bb37-0242ac130002 |")
        self.assertEqual(captured.records[6].getMessage(), "| requirement | ### 2.1 Information listed by treqs list | 63ef8bfa76ae11ebb811cf2f044815f7 |")

    # <treqs-element id="3448f8f8a23411eba9dca7925d1c5fe9" type="unittest">
    # Basic listing.
    # <treqs-link type="tests" target="437f09c6-9613-11ea-bb37-0242ac130002" />
    # </treqs-element>
    def test_list_elements_of_type(self):
        with self.assertLogs('treqs-on-git.treqs-ng', level='INFO') as captured:
            with self.assertRaises(SystemExit) as cm:
                le = list_elements()
                le.list_elements("information", './requirements/2-list-reqts.md', 'false', None, False, False)

        self.assertEqual(cm.exception.code, 0)
        self.assertEqual(len(captured.records), 6)
        self.assertEqual(captured.records[0].getMessage(), "file_traverser created")
        self.assertEqual(captured.records[1].getMessage(), "treqs_element_fatory created")
        self.assertEqual(captured.records[2].getMessage(), "list_elements created")
        self.assertEqual(captured.records[3].getMessage(), "Calling XML traversal with filename ./requirements/2-list-reqts.md")
        self.assertEqual(captured.records[4].getMessage(), "\n\n ### Processing elements in File ./requirements/2-list-reqts.md")
        self.assertEqual(captured.records[5].getMessage(), "| information | Note that the type should usually be defined in the TIM. treqs list does however not check for this to be the case. Use treqs check instead to make sure that all types are consistent with the TIM. treqs list allows to search for invalid types. | abc40962a23511eba9dca7925d1c5fe9 |")

    # <treqs-element id="3448f8f8a23411eba9dca7925d1c5fe9" type="unittest">
    # Basic listing.
    # <treqs-link type="tests" target="437f09c6-9613-11ea-bb37-0242ac130002" />
    # </treqs-element>
    def test_list_elements_with_id(self):
        with self.assertLogs('treqs-on-git.treqs-ng', level='INFO') as captured:
            with self.assertRaises(SystemExit) as cm:
                le = list_elements()
                le.list_elements(None, './requirements/2-list-reqts.md', 'false', 'a0820b4a-9614-11ea-bb37-0242ac130002', False, False)

        self.assertEqual(cm.exception.code, 0)
        self.assertEqual(len(captured.records), 6)
        self.assertEqual(captured.records[0].getMessage(), "file_traverser created")
        self.assertEqual(captured.records[1].getMessage(), "treqs_element_fatory created")
        self.assertEqual(captured.records[2].getMessage(), "list_elements created")
        self.assertEqual(captured.records[3].getMessage(), "Calling XML traversal with filename ./requirements/2-list-reqts.md")
        self.assertEqual(captured.records[4].getMessage(), "\n\n ### Processing elements in File ./requirements/2-list-reqts.md")
        self.assertEqual(captured.records[5].getMessage(), "| requirement | ### 2.3 Filter by ID | a0820b4a-9614-11ea-bb37-0242ac130002 |")


    # <treqs-element id="" type="unittest">
    # List outgoing tracelinks.
    # <treqs-link type="tests" target="1595ed20a27111eb8d3991dd3edc620a" />
    # </treqs-element>
    def test_list_elements_with_outgoing_links(self):
        with self.assertLogs('treqs-on-git.treqs-ng', level='INFO') as captured:
            with self.assertRaises(SystemExit) as cm:
                le = list_elements()
                # Some caching problems for this test, I assume..
                le.treqs_element_factory._treqs_elements.clear()
                le.list_elements(None, './requirements/2-list-reqts.md', 'false', '1595ed20a27111eb8d3991dd3edc620a', True, False)

        self.assertEqual(cm.exception.code, 0)
        self.assertEqual(len(captured.records), 11)
        self.assertEqual(captured.records[0].getMessage(), "file_traverser created")
        self.assertEqual(captured.records[1].getMessage(), "treqs_element_fatory created")
        self.assertEqual(captured.records[2].getMessage(), "list_elements created")
        self.assertEqual(captured.records[3].getMessage(), "Calling XML traversal with filename ./requirements/2-list-reqts.md")
        self.assertEqual(captured.records[4].getMessage(), "\n\n ### Processing elements in File ./requirements/2-list-reqts.md")
        self.assertEqual(captured.records[5].getMessage(), "| requirement | ### 2.6 List outgoing tracelinks | 1595ed20a27111eb8d3991dd3edc620a |")
        self.assertEqual(captured.records[6].getMessage(), "| --outlink--> (hasParent) | TREQS ELEMENT NOT FOUND. PERHAPS ITS FILE WAS NOT INCLUDED IN THIS LIST? | 35590bca-960f-11ea-bb37-0242ac130002 |")
        self.assertEqual(captured.records[7].getMessage(), "| --outlink--> (relatesTo) | ### 2.1 Information listed by treqs list | 63ef8bfa76ae11ebb811cf2f044815f7 |")
        # TODO #9 This is not the desired behavior. We should first go through all the files and then log.
        self.assertEqual(captured.records[8].getMessage(), "| --outlink--> (relatesTo) | TREQS ELEMENT NOT FOUND. PERHAPS ITS FILE WAS NOT INCLUDED IN THIS LIST? | d9e68f9aa27b11eb8d3991dd3edc620a |")
        self.assertEqual(captured.records[9].getMessage(), "| --outlink--> (addresses) | TREQS ELEMENT NOT FOUND. PERHAPS ITS FILE WAS NOT INCLUDED IN THIS LIST? | 1e9885f69d3311eb859fc4b301c00591 |")
        self.assertEqual(captured.records[10].getMessage(), "| --outlink--> (addresses) | TREQS ELEMENT NOT FOUND. PERHAPS ITS FILE WAS NOT INCLUDED IN THIS LIST? | 54a4e59a9d3311ebb4d2c4b301c00591 |")


    # <treqs-element id="" type="unittest">
    # Test listing incoming tracelinks.
    # <treqs-link type="tests" target="d9e68f9aa27b11eb8d3991dd3edc620a" />
    # </treqs-element>
    def test_list_elements_with_incoming_links(self):
        with self.assertLogs('treqs-on-git.treqs-ng', level='INFO') as captured:
            with self.assertRaises(SystemExit) as cm:
                le = list_elements()
                le.list_elements(None, './requirements/2-list-reqts.md', 'false', 'd9e68f9aa27b11eb8d3991dd3edc620a', False, True)

        self.assertEqual(cm.exception.code, 0)
        self.assertEqual(len(captured.records), 7)
        self.assertEqual(captured.records[0].getMessage(), "file_traverser created")
        self.assertEqual(captured.records[1].getMessage(), "treqs_element_fatory created")
        self.assertEqual(captured.records[2].getMessage(), "list_elements created")
        self.assertEqual(captured.records[3].getMessage(), "Calling XML traversal with filename ./requirements/2-list-reqts.md")
        self.assertEqual(captured.records[4].getMessage(), "\n\n ### Processing elements in File ./requirements/2-list-reqts.md")
        self.assertEqual(captured.records[5].getMessage(), "| requirement | ### 2.7 List incoming tracelinks | d9e68f9aa27b11eb8d3991dd3edc620a |")
        self.assertEqual(captured.records[6].getMessage(), "| --inlink--> (relatesTo) | ### 2.6 List outgoing tracelinks | 1595ed20a27111eb8d3991dd3edc620a |")
if __name__ == "__main__":
    unittest.main()
