import unittest

from check_elements import check_elements

class TestCheckElements(unittest.TestCase):

    # <treqs-element id="e9db9d24920d11eba831f018989356c1" type="unittest">
    # setup and successful checks of ../requirements/5-test-faulty-types-and-links.md
    # <treqs-link type="tests" target="e770de36920911eb9355f018989356c1" />
    # <treqs-link type="tests" target="2c600896920a11ebbb6ff018989356c1" />
    # <treqs-link type="tests" target="56cbd2e0920a11ebb9d1f018989356c1" />
    # </treqs-element>
    def test_check_elements(self):
        with self.assertLogs('treqs-on-git.treqs-ng', level='INFO') as captured:
            with self.assertRaises(SystemExit) as cm:
                ce = check_elements()
                ce.check_elements('./tests/test_data/5-test-faulty-types-and-links.md', 'false', './ttim.json')
        
        self.assertEqual(cm.exception.code, 1)

        self.assertEqual(len(captured.records), 10)
        self.assertEqual(captured.records[0].getMessage(), "file_traverser created")
        self.assertEqual(captured.records[1].getMessage(), "check_elements created")
        self.assertEqual(captured.records[2].getMessage(), "Processing TTIM at ./ttim.json")
        self.assertEqual(captured.records[3].getMessage(), "Types and their links according to TTIM: {'requirement': ['relatesTo', 'hasParent', 'addresses'], 'stakeholder-need': [], 'stakeholder-requirement': ['addresses', 'relatesTo'], 'unittest': ['relatesTo', 'hasParent', 'tests'], 'information': ['relatesTo']}")
        self.assertEqual(captured.records[4].getMessage(), "Calling XML traversal with filename ./tests/test_data/5-test-faulty-types-and-links.md")
        self.assertEqual(captured.records[6].getMessage(), "Unrecognized type: non-existing-type (File ./tests/test_data/5-test-faulty-types-and-links.md)")
        self.assertEqual(captured.records[7].getMessage(), "Unrecognized link type relatesToo within an element of type requirement (File ./tests/test_data/5-test-faulty-types-and-links.md)")
        self.assertEqual(captured.records[8].getMessage(), "Unrecognized link type tests within an element of type requirement (File ./tests/test_data/5-test-faulty-types-and-links.md)")

if __name__ == "__main__":
    unittest.main()
