import unittest

from process_elements import process_elements

class TestProcessElements(unittest.TestCase):

    # <treqs-element id="aec9386489c911eb99c7c4b301c00591" type="unittest">
    # process plantuml test add picture link
    # <treqs-link type="tests" target="9a8a627687f111eb9d1ec4b301c00591" />
    # </treqs-element>
    def test_create_plantuml_links(self):
        pe = process_elements()
        lines = []
        lines.append("@startuml testdiagram\n")
        lines.append("crazy plantUML content\n")
        lines.append("@enduml\n")
        lines.append("\n")
        self.assertEqual(4, len(lines))
        newlines = pe.create_plantuml_links(lines)
        self.assertEqual(5, len(newlines))
        self.assertEqual("@enduml\n", newlines[2])
        self.assertEqual("![testdiagram](testdiagram.png)\n", newlines[3])
        self.assertEqual("\n", newlines[4])

    # <treqs-element id="0d3065e489ca11ebb2ccc4b301c00591" type="unittest">
    # process plantuml test picture link exists
    # <treqs-link type="tests" target="9a8a627687f111eb9d1ec4b301c00591" />
    # </treqs-element>
    def test_create_plantuml_links_link_exists(self):
        pe = process_elements()
        lines = ["@startuml testdiagram\n", "crazy plantUML content\n", "@enduml\n", "![testdiagram](testdiagram.png)\n","\n"]
        self.assertEqual(5, len(lines))
        newlines = pe.create_plantuml_links(lines)
        self.assertEqual(5, len(newlines))
        self.assertEqual("@enduml\n", newlines[2])
        self.assertEqual("![testdiagram](testdiagram.png)\n", newlines[3])
        self.assertEqual("\n", newlines[4])

if __name__ == "__main__":
    unittest.main()
