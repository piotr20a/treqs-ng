<treqs>

<treqs-element id="9a8a627687f111eb9d1ec4b301c00591" type="requirement">

### 4.1 Process plantuml models
TReqs shall create and link or update embedded PlantUML. Example:

```
treqs process --plantuml example.md 
```

This command shall process all plantuml models in example.md.

The following code block thus should create a png file in the same directory as this file, the name should be `Sample.png` as derived from the label in the first line. Just behind the model, a line should be added that embeds the generated picture in this file. If that line already exists, no line will be generated.

```
@startuml Sample
Alice -> Bob: test
@enduml
![Sample](Sample.png)
```

treqs process shall work as depicted in the following activity diagram:

@startuml process_plantuml
start                                                                                    
:Generate png files for each plantuml block;                                             
:Create in-line markdown reference to png file;                                          
if (in-line markdown reference exists) then (no)                                         
  :add in-line markdown reference  
after plantuml block;
endif
stop
@enduml 
![process_plantuml](process_plantuml.png)

</treqs-element>

<treqs-element id="8793c8ca96eb11ebaca5c4b301c00591" type="requirement">

### 4.2 MIT License

TReqs shall be available under MIT license. It is therefore preferred, that libraries that we use are also using MIT license. 
</treqs-element>

<treqs-element id="c7e7946a96eb11ebbfd9c4b301c00591" type="information">

Note that [plantuml is available under several licenses](https://plantuml.com/faq#ddbc9d04378ee462). It appears that very little is lost when taking the MIT licensed version (Just their visual text syntax, for which it is doubtful whether it scales for the purpose in treqs). 

<treqs-link type="relatesTo" target="8793c8ca96eb11ebaca5c4b301c00591" />
</treqs-element>

</treqs>
