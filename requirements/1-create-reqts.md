<treqs>
<treqs-element id="39f253a076ae11ebb811cf2f044815f7" type="requirement">

### 1.0 Parameters and default output of treqs create
TReqs shall allow to generate treqs elements via the create command.
</treqs-element>

<treqs-element id="4d7ca13c76ae11ebb811cf2f044815f7" type="requirement">

### 1.1 Unique IDs
TReqs shall assign a unique ID to each newly created element.
</treqs-element>

<treqs-element id="dab6fe5876cc11ebb811cf2f044815f7" type="requirement">

### 1.3 Create links

treqs createlink shall prompt the user for the input needed for a treqs-link. It also allows to provide the input as command line options. It will print a valid link to the command line that then can be inserted into the treqs element that owns this link.

<treqs-link type="addresses" target="1e9885f69d3311eb859fc4b301c00591" />
</treqs-element>

</treqs>
