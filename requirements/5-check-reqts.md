<treqs>
<treqs-element id="c5402c1a919411eb8311f018989356c1" type="requirement">

### 5.0 Parameters and default output of treqs check

    Usage: treqs check [OPTIONS] [FILENAME]
    
      Check treqs elements in FILENAME. This defaults to '.', i.e. this folder.
    
    Options:
      --recursive Boolean  List treqs elements recursively in all subfolders.
      --ttim TEXT          Path to a type and traceability information model (TTIM) in json format.
      --help               Show this message and exit.

The default value for recursive is 'true'. Unless otherwise specified, treqs shall list elements recursively.

FILENAME can either provide a directory or a file. If FILENAME is omitted, treqs defaults to '.', i.e. the current working directory.

TTIM points to a json file that contains a type and trace information model (TTIM). The default is ./ttim.json. Currently, the only relevant information in the TTIM file is an array containing all types and their relationships. Consult the sample file ttim.json for a usage example.
<treqs-link type="addresses" target="7438f4c69d3011ebb63fc4b301c00591" />
<treqs-link type="addresses" target="3d0063ae9d3511eb898ec4b301c00591" />
<treqs-link type="addresses" target="17927fb4b89511ebbf8411e42223e21f" />
> Does JSON allow for comments and hints?
</treqs-element>

<treqs-element id="3cff0d2a919511eb8becf018989356c1" type="requirement">

### 5.1 Check for unrecognised types.

When checking treqs elements, treqs shall report elements that have types not listed in the TTIM. Additionally, treqs shall report the file in which this violation occured.

> Example:

VIOLATION: Unrecognized type: SR (File ./requirements/treqs-system-requirements.md)

<treqs-link type="hasParent" target="c5402c1a919411eb8311f018989356c1" />
</treqs-element>

<treqs-element id="951ecc70919511eb978ff018989356c1" type="requirement">

### 5.2 Check for unrecognised link types.

When checking treqs links, treqs shall report links that have types not listed for the parent element in the TTIM. Additionally, treqs shall report the file in which this violation occured.

> Example:

VIOLATION: Unrecognized link type tests within an element of type unittest (File ./requirements/test_req.md)
<treqs-link type="hasParent" target="c5402c1a919411eb8311f018989356c1" />
<treqs-link type="addresses" target="d15209ca9d3211eba2e0c4b301c00591" />
<treqs-link type="addresses" target="1e9885f69d3311eb859fc4b301c00591" />
<treqs-link type="addresses" target="54a4e59a9d3311ebb4d2c4b301c00591" />
<treqs-link type="addresses" target="9956605cb89411ebbf8411e42223e21f" />
</treqs-element>

<treqs-element id="b4d30bec919711eba4e1f018989356c1" type="requirement">

### 5.3 Check for missing links.

When checking treqs elements, treqs shall report links that are required according to the TTIM, but missing in the element. Additionally, treqs shall report the file in which this violation occured.

> Example:

VIOLATION: Links missing for ec5d1aa4915511eb9295f018989356c1: ['tests'] (File ./requirements//test_req.md)
<treqs-link type="hasParent" target="c5402c1a919411eb8311f018989356c1" />
<treqs-link type="addresses" target="d15209ca9d3211eba2e0c4b301c00591" />
<treqs-link type="addresses" target="1e9885f69d3311eb859fc4b301c00591" />
<treqs-link type="addresses" target="54a4e59a9d3311ebb4d2c4b301c00591" />
<treqs-link type="addresses" target="17927fb4b89511ebbf8411e42223e21f" />
</treqs-element>
</treqs>
