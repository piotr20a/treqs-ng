<treqs>
<treqs-element id="a0820e06-9614-11ea-bb37-0242ac130002" type="requirement" >

### 2.0 Parameters and default output of treqs list

    Usage: treqs list [OPTIONS] [FILENAME]
    
      List treqs elements in FILENAME
    
    Options:
      --type TEXT          Limit action to specified treqs element type
      --recursive BOOLEAN  List treqs elements recursively in all subfolders.
      --help               Show this message and exit.

If type is omitted, all treqs elements, regardless of type, will be returned.

The default value for recursive is 'true'. Unless otherwise specified, treqs shall list elements recursively.

FILENAME can either provide a directory or a file. If FILENAME is omitted, treqs defaults to '.', i.e. the current working directory.
<treqs-link type="hasParent" target="35590bca-960f-11ea-bb37-0242ac130002" />
<treqs-link type="addresses" target="1e9885f69d3311eb859fc4b301c00591" />

</treqs-element>
<treqs-element id="63ef8bfa76ae11ebb811cf2f044815f7" type="requirement">

### 2.1 Information listed by treqs list

When listing treqs-elements, treqs shall list the following information as a markdown table:

  - element type, 
  - the label (i.e. the first non-empty line of a treqs element), and 
  - the id of the element (the UID provided by the treqs create command). 

> Example:

| Element type | Label | UID |
| :--- | :--- | :--- |
| requirement | ### 2.0 Parameters and default output of treqs list | a0820e06-9614-11ea-bb37-0242ac130002 |
| requirement | ### 2.1 Filter by type  | 437f09c6-9613-11ea-bb37-0242ac130002 |
| requirement | ### 2.2 Filter by ID  | a0820b4a-9614-11ea-bb37-0242ac130002 |
| requirement | ### 2.3 List all elements in a file | bc89e02a76c811ebb811cf2f044815f7 |
| requirement | ### 2.4 List treqs elements in a directory | 638fa22e76c911ebb811cf2f044815f7 |

<treqs-link type="hasParent" target="35590bca-960f-11ea-bb37-0242ac130002" />
<treqs-link type="addresses" target="1e9885f69d3311eb859fc4b301c00591" />

</treqs-element>

<treqs-element id="437f09c6-9613-11ea-bb37-0242ac130002" type="requirement" >

### 2.2 Filter by type 
TReqs shall allow to list treqs-elements within a specified gitlab project that have a specific type (e.g. requirement, test, quality requirement).
<treqs-link type="hasParent" target="35590bca-960f-11ea-bb37-0242ac130002" />
<treqs-link type="addresses" target="1e9885f69d3311eb859fc4b301c00591" />

> Example:

    treqs list --type=requirement

</treqs-element>

<treqs-element id="abc40962a23511eba9dca7925d1c5fe9" type="information">
Note that the type should usually be defined in the TIM. treqs list does however not check for this to be the case. Use treqs check instead to make sure that all types are consistent with the TIM. treqs list allows to search for invalid types.
</treqs-element>


<treqs-element id="a0820b4a-9614-11ea-bb37-0242ac130002" type="requirement" >

### 2.3 Filter by ID
TReqs shall allow to list treqs-elements within a specified gitlab project that have a specific ID.
<treqs-link type="hasParent" target="35590bca-960f-11ea-bb37-0242ac130002" />
<treqs-link type="addresses" target="1e9885f69d3311eb859fc4b301c00591" />
<treqs-link type="addresses" target="54a4e59a9d3311ebb4d2c4b301c00591" />

> Example:

    treqs list --uid "35590bca-960f-11ea-bb37-0242ac130002"

</treqs-element>

<treqs-element id="bc89e02a76c811ebb811cf2f044815f7" type="requirement">

### 2.4 List all elements in a file

If a file is given, treqs will list all treqs elements in that file.
<treqs-link type="hasParent" target="35590bca-960f-11ea-bb37-0242ac130002" />
<treqs-link type="relatesTo" target="a0820e06-9614-11ea-bb37-0242ac130002" />
</treqs-element>

<treqs-element id="638fa22e76c911ebb811cf2f044815f7" type="requirement">


### 2.5 List treqs elements in a directory

If a directory is given, treqs will list all treqs elements in all files of this directory. 
If the recursive parameter is given, treqs will list also all treqs elements in all subdirectories of the given directory.
<treqs-link type="hasParent" target="35590bca-960f-11ea-bb37-0242ac130002" />
<treqs-link type="relatesTo" target="bc89e02a76c811ebb811cf2f044815f7" />
</treqs-element>

<treqs-element id="1595ed20a27111eb8d3991dd3edc620a" type="requirement">

### 2.6 List outgoing tracelinks

Optionally, treqs list shall list the outgoing tracelinks of each treqs element. This list shall be integrated in the table of the list command. Labels of target treqs-elements shall be shown, if available (i.e. the current list command includes the file in which the target is specified).

> Example:
> The following command will result in the following output, since the parent of this requirement is specified in a different file.

    treqs list --outlinks --uid "1595ed20a27111eb8d3991dd3edc620a" --filename ./requirements/2-list-reqts.md

| Element type | Label | UID |
| :--- | :--- | :--- |
| requirement | ### 2.6 List outgoing tracelinks | 1595ed20a27111eb8d3991dd3edc620a |
| --outlink--> (hasParent) | TREQS ELEMENT NOT FOUND. PERHAPS ITS FILE WAS NOT INCLUDED IN THIS LIST? | 35590bca-960f-11ea-bb37-0242ac130002 |
| --outlink--> (relatesTo) | ### 2.1 Information listed by treqs list | 63ef8bfa76ae11ebb811cf2f044815f7 |

> at the same time, the following list command includes all requirements and therefore produces the desired result:

    treqs list --outlinks --uid "1595ed20a27111eb8d3991dd3edc620a" --filename ./requirements

| Element type | Label | UID |
| :--- | :--- | :--- |
| requirement | ### 2.6 List outgoing tracelinks | 1595ed20a27111eb8d3991dd3edc620a |
| --outlink--> (hasParent) | ## 2. List TReqs Elements | 35590bca-960f-11ea-bb37-0242ac130002 |
| --outlink--> (relatesTo) | ### 2.1 Information listed by treqs list | 63ef8bfa76ae11ebb811cf2f044815f7 |

<treqs-link type="hasParent" target="35590bca-960f-11ea-bb37-0242ac130002" />
<treqs-link type="relatesTo" target="63ef8bfa76ae11ebb811cf2f044815f7" />
<treqs-link type="relatesTo" target="d9e68f9aa27b11eb8d3991dd3edc620a" />
<treqs-link type="addresses" target="1e9885f69d3311eb859fc4b301c00591" />
<treqs-link type="addresses" target="54a4e59a9d3311ebb4d2c4b301c00591" />

</treqs-element>

<treqs-element id="d9e68f9aa27b11eb8d3991dd3edc620a" type="requirement">

### 2.7 List incoming tracelinks

Optionally, treqs list shall list the incoming tracelinks of each treqs element. This list shall be integrated in the table of the list command. Labels of target treqs-elements shall be shown, if available (i.e. the current list command includes the file in which the target is specified).

    treqs list --inlinks --uid "d9e68f9aa27b11eb8d3991dd3edc620a" --filename ./requirements

| Element type | Label | UID |
| :--- | :--- | :--- |
| requirement | ### 2.7 List incoming tracelinks | d9e68f9aa27b11eb8d3991dd3edc620a |
| --inlink--> (relatesTo) | ### 2.6 List outgoing tracelinks | 1595ed20a27111eb8d3991dd3edc620a |

<treqs-link type="hasParent" target="35590bca-960f-11ea-bb37-0242ac130002" />
<treqs-link type="relatesTo" target="63ef8bfa76ae11ebb811cf2f044815f7" />
<treqs-link type="addresses" target="1e9885f69d3311eb859fc4b301c00591" />
<treqs-link type="addresses" target="54a4e59a9d3311ebb4d2c4b301c00591" />

</treqs-element>
</treqs>
