<treqs>

## P1 Put stakeholders' information needs and goals of traceability at the center.

<treqs-element id="63d7b3ce9d3011eb9669c4b301c00591" type="stakeholder-need">

### P1.1 Cross-organisational TIM
The traceability information model covers the needs of different stakeholders, i.e., it can be applied by different teams and allows to capture different perspectives within the organisations.
</treqs-element>

<treqs-element id="7438f4c69d3011ebb63fc4b301c00591" type="stakeholder-requirement">

#### REQ-CT-P1.1-1 Explicit TIM
As a senior requirements engineer, I want to define an explicit Traceability Information Model (TIM) so that the needs and perspectives of different stakeholders are covered.

<treqs-link type="addresses" target="63d7b3ce9d3011eb9669c4b301c00591" />
<treqs-link type="addresses" target="7eb41926b89211ebbf8411e42223e21f" />
</treqs-element>


<treqs-element id="9e19476e9d3011ebb954c4b301c00591" type="stakeholder-requirement">

#### REQ-CT-P1.1-2 Support for links to other tools and organizations

As a senior requirements engineer, I want to link from treqs-elements to artifacts in other tools or organizations so that I can indicate dependencies accross boundaries.
<treqs-link type="addresses" target="63d7b3ce9d3011eb9669c4b301c00591" />

> Our initial version supported linking to github issue tracker. We can build that again, but while this is an external tool, I wonder whether it is a good place for high-level/stakeholder requirements.

</treqs-element>


<treqs-element id="c3587b7a9d3111eb8736c4b301c00591" type="stakeholder-need">

### P1.2 Customized traceability documentation

The rationale for traceability as well as concrete procedures should be recorded for each relevant role. Documentation includes traceability benefits for each role and is made available to all engineers.
</treqs-element>

> Project specific Traceability Documentation probably not part of treqs?

<treqs-element id="dcbc45b09d3111eb8f0cc4b301c00591" type="stakeholder-need">

### P1.3 Just enough traceability

There are clear guidelines for when it is appropriate to introduce a trace link. These guidelines also define the level of granularity.

</treqs-element>

<treqs-element id="d15209ca9d3211eba2e0c4b301c00591" type="stakeholder-requirement">

#### REQ-CT-P1.3-1 Enforce just enough traceability

treqs shall allow to define which links in the TIM are strictly necessary and which links are not allowed.

<treqs-link type="addresses" target="dcbc45b09d3111eb8f0cc4b301c00591" />
<treqs-link type="addresses" target="7eb41926b89211ebbf8411e42223e21f" />
</treqs-element>


<treqs-element id="0ddb55469d3211eb9d31c4b301c00591" type="stakeholder-need">

### P1.4 Upfront traceability

Invest in creating a high-quality set of trace links upfront that then only needs to be maintained going forward.

</treqs-element>

<treqs-element id="1e9885f69d3311eb859fc4b301c00591" type="stakeholder-requirement">

#### REQ-CT-P1.4-1 Support upfront traceability

treqs shall support the creation of a high-quality set of tracelinks in one session.
<treqs-link type="addresses" target="0ddb55469d3211eb9d31c4b301c00591" />
<treqs-link type="addresses" target="7eb41926b89211ebbf8411e42223e21f" />
</treqs-element>

<treqs-element id="54a4e59a9d3311ebb4d2c4b301c00591" type="stakeholder-requirement">

#### REQ-CT-P1.4-2 Support tracelink maintenance

treqs shall support continuous collaborative traceability maintenance.

- Provide automated checks of tracelinks
- Provide review support (provided through git infrastructure)

<treqs-link type="addresses" target="0ddb55469d3211eb9d31c4b301c00591" />
<treqs-link type="addresses" target="7eb41926b89211ebbf8411e42223e21f" />
</treqs-element>

<treqs-element id="156db7549d3211eb8754c4b301c00591" type="stakeholder-need">

### P1.5 Persisted trace rationale

The traceability information model contains information about the motivation for tracing, e.g., by naming the different link types accordingly (such as "tested by" or "conflicts with").

</treqs-element>

<treqs-element id="3d0063ae9d3511eb898ec4b301c00591" type="stakeholder-requirement">

#### REQ-CT-P1.5-1 Comments and hints in TIM

treqs shall allow to add comment and hints to the TIM to make the rationale explicit. These should also be exploited by treqs check to provide feedback.
<treqs-link type="addresses" target="156db7549d3211eb8754c4b301c00591" />
</treqs-element>


<treqs-element id="1d7735929d3211ebbdb3c4b301c00591" type="stakeholder-need">

### P1.6 Traceability training

All engineers that work with traceable artifacts and/or trace links receive training in traceability activities, including how and when to create links, how and when to maintain them, and how trace links can be used to facilitate recurring development activities. Teaching tools (e.g., videos) can be used for this purpose. 

> Project specific training probably not part of treqs? But perhaps one could provide templates and man pages? Traceability training specific for treqs?
</treqs-element>


## P2 Balance the effort and benefit of traceability management per role.

<treqs-element id="ce2f94ac9d3611ebad59c4b301c00591" type="stakeholder-need">

### P2.1 Traceability dashboards

A dashboard provides continuously updated information about relevant data about traceability such as traceability coverage as well as access to traceability information in the form of graphs and grids.

> Several requirements hidden here: The dashboard is likely an external tool. treqs should produce graphs, grids, and reports with statistics.
</treqs-element>


<treqs-element id="9cd9a996b86c11ebbf8411e42223e21f" type="stakeholder-requirement">

#### REQ-CT-P2.1-1 Traceability coverage report
<treqs-link type="addresses" target="ce2f94ac9d3611ebad59c4b301c00591" />
<treqs-link type="addresses" target="7eb41926b89211ebbf8411e42223e21f" />
</treqs-element>

<treqs-element id="ba75e2deb86d11ebbf8411e42223e21f" type="stakeholder-requirement">

#### REQ-CT-P2.1-2 Generate traceability graph
<treqs-link type="addresses" target="ce2f94ac9d3611ebad59c4b301c00591" />
<treqs-link type="addresses" target="7eb41926b89211ebbf8411e42223e21f" />
</treqs-element>

<treqs-element id="d6c339a29d3611eb83fac4b301c00591" type="stakeholder-need">

#### P2.2 Traceability accountability 

Traceability activities are included in the general responsibilities of engineers. Contributions to traceability are part of the criteria used for individual performance reviews.

> Something to consider. Should statistics per user be provided? Git blame is certainly an interesting tool for that.
</treqs-element>

<treqs-element id="fd63aef0b86d11ebbf8411e42223e21f" type="stakeholder-requirement">

#### REQ-CT-P2.2-1 Generate history of changes to treqs-elements and treqs-links

As a treqs user I want to see who has changed a requirement or tracelink in what way so that I avoid re-introducing old problems.
<treqs-link type="addresses" target="d6c339a29d3611eb83fac4b301c00591" />
<treqs-link type="addresses" target="7eb41926b89211ebbf8411e42223e21f" />
</treqs-element>

## P3 Enable change propagation and notification across boundaries.

<treqs-element id="28c70d70b89211ebbf8411e42223e21f" type="stakeholder-need">

### P3.1 Implicit traceability 

Trace links are based on the use of unique identifiers or specific naming conventions. Refering to an ID in a different artifact or using the name of an artifact in another implies that there is a relationship between them.

> We kind of have those through joint commits. We can also use gitlab standards to refer to issues, commits, files, ... Should requirements also get a naming convention?
</treqs-element>

<treqs-element id="7eb41926b89211ebbf8411e42223e21f" type="stakeholder-need">

### P3.2 Tool support for traceability management

A tool (chain) is used that allows to create and maintain trace links. It either integrates different tools for the management of artifacts or centrally stores all development artifacts.

> I think by fulfilling all of the above, treqs is such a tool. But when do we judge this as fulfilled?
</treqs-element>


<treqs-element id="135bf6ceb89411ebbf8411e42223e21f" type="stakeholder-need">

### P3.3 Traceability to versions of artifacts

For each development artifact, versions are defined at specific points of the development lifecycle (e.g., baselines). Critical changes to these versions are traced.

> Deserves some thinking. We could definitely point towards HEAD or specific other commits. It could be nice to have a mechanism to create a link to the commit in which the link was introduced...
</treqs-element>


<treqs-element id="3adab348b89411ebbf8411e42223e21f" type="stakeholder-need">

### P3.4 Communication of changes

The standard procedures of the organisation contain clearly defined communication pathways to inform all relevant stakeholders of changes that affect traceable artifacts. Possible pathways are face-to-face, email, or automated change notifications from a traceability tool.

> Reviews of merge requests,  infrastructure for change notifications in the git ecosystem
</treqs-element>

## P4 Strive for a rigorous culture with respect to traceability maintenance.

<treqs-element id="63990366b89411ebbf8411e42223e21f" type="stakeholder-need">

### P4.1 Governance ambassadors 
A specific role in the organisation is responsible for ensuring quality and integrity of the trace links. This key stakeholder meets with the teams, reviews trace links, and drives the strategic and technical evolution of traceability in the organisation.

> We should have this role in mind when designing traceabilty support. It also relates to training and recommendations.
</treqs-element>


<treqs-element id="9956605cb89411ebbf8411e42223e21f" type="stakeholder-need">

### P4.2 Traceability Acceptance Criteria

Creating necessary links and updating them is part of acceptance criteria and checked in reviews. Increments are only accepted by all stakeholders if and when trace links are in place and up-to-date.

> treqs encourages that. Perhaps there is a way to actually provide templates for reviews, e.g. based on definition of done? 
> definitely, our checks aim to support that.
</treqs-element>

<treqs-element id="17927fb4b89511ebbf8411e42223e21f" type="stakeholder-requirement">

#### REQ-CT-P4.2-1 Allow definition of traceability acceptance criteria 
As a system manager I want to define criteria for acceptable quality of tracelinks so that developers have clear guidance when updating, creating, or reviewing tracelinks.
<treqs-link type="addresses" target="9956605cb89411ebbf8411e42223e21f" />
</treqs-element>

</treqs>
