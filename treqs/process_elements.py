import os
from pkg_resources import resource_filename
import logging

from file_traverser import file_traverser

class process_elements:

    def __init__(self):
        self.logger = logging.getLogger('treqs-on-git.treqs-ng')
        self.traverser = file_traverser()
        self.logger.info('process_elements created')

    def process_plantuml(self, file_name, recursive):
        self.logger.info('process_plantuml started')
        self.traverser.traverse_file_hierarchy(file_name, recursive, self.process_plantuml_file)

    def process_plantuml_file(self, filename):
        self.make_plantuml_figures(filename)
        self.create_plantuml_links_infile(filename)
        
    # Go through the file and create PNG files for each plantuml diagram. 
    # Future versions may rely on online services, so that no dependency to Java is needed. It is for example possible to create the figures on the fly via a webservice, so that this method does not have to do anything.
    def make_plantuml_figures(self, filename):
        #libdir = resource_filename('treqs', 'lib/plantuml.1.2021.2.jar')
        libdir = os.path.split(__file__)[0] + "/../lib/plantuml.jar"
        self.logger.info('Using plantuml binary at %s', libdir)
        os.system('java -jar %s  %s' % (libdir, filename))


    # Go through file and make sure that each plantuml block is followed by a link to the generated png file
    # Future versions may offer a local and a webservice based link
    def create_plantuml_links_infile(self, filename):
        try:
            with open(filename) as f:
                newlines = self.create_plantuml_links(f.readlines()) 

            # Now, we need to overwrite the file
            with open(filename,'w') as f:
                f.writelines(newlines)
        except UnicodeDecodeError:
            print("Not a text file?")
            pass

    def create_plantuml_links(self, lines):
        newlines = []
        reachedEnd = False
        for l in lines:
            if reachedEnd:
                # last line ended a uml block
                reachedEnd = False
                picref = "![%s](%s.png)\n" % (name,name)
                if (l != picref):
                    self.logger.info('Adding line: %s', picref)
                    newlines.append(picref)
            newlines.append(l)
            if l.startswith("@startuml"):
                name = l.split(" ", 1)[1].replace("\n","")
                self.logger.info('%s', name) 
            reachedEnd = l.startswith("@enduml")

        self.logger.info('Old lines: %i, new lines %i', len(lines), len(newlines))
        return newlines

if __name__ == "__main__":
    pe = process_elements()
    # pe.extract_plant_uml("../requirements/4-process-elements.md")
    pe.create_plantuml_links_infile("../requirements/4-process-elements.md")
