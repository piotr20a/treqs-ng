import os
import xml.etree.cElementTree as ET
import logging
import sys

from treqs_element import *
from file_traverser import file_traverser

class list_elements:


    def __init__(self):
        self.logger = logging.getLogger('treqs-on-git.treqs-ng')
        self.traverser = file_traverser()
        self.treqs_element_factory = treqs_element_factory()
        self.logger.info('list_elements created')

    def list_elements(self, treqs_type, file_name, recursive, uid, outlinks,inlinks):
         self.treqs_type = treqs_type
         self.uid = uid
         self.outlinks = outlinks
         self.inlinks = inlinks
         self.traverser.traverse_file_hierarchy(file_name, recursive, self.log_element_info, self.traverser.traverse_XML_file)
         sys.exit(0)
        
    def log_element_info(self, file_name, element):
        te = self.treqs_element_factory.get_treqs_element(element)

        # Filter by type
        if (self.treqs_type != None and self.treqs_type != te.treqs_type):
            return 
        # Filter by ID
        if (self.uid != None and self.uid != te.uid):
            return 
        self.logger.info('| %s | %s | %s |', te.treqs_type, te.label, te.uid)
        if (self.outlinks):
            for tl in te.outlinks:
                target_treqs_type = self.treqs_element_factory._treqs_elements.get(tl.target)
                if target_treqs_type == None:
                    label = 'TREQS ELEMENT NOT FOUND. PERHAPS ITS FILE WAS NOT INCLUDED IN THIS LIST?'
                else:
                    label = target_treqs_type.label
                self.logger.info('| --outlink--> (%s) | %s | %s |', tl.tlt, label, tl.target)

        if (self.inlinks):
            self.treqs_element_factory.process_inlinks()
            for tl in te.inlinks:
                source_te = self.treqs_element_factory._treqs_elements.get(tl.source)
                if source_te == None:
                    label = 'TREQS ELEMENT NOT FOUND. PERHAPS ITS FILE WAS NOT INCLUDED IN THIS LIST?'
                else:
                    label = source_te.label
                self.logger.info('| --inlink--> (%s) | %s | %s |', tl.tlt, label, tl.source)



if __name__ == "__main__":
   le = list_elements()
   le.list_elements("../requirements/treqs-system-requirements.md", "true")
