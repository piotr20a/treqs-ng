from create_elements import create_elements
from list_elements import list_elements
from check_elements import check_elements
from process_elements import process_elements

import logging
import click

@click.group()
def treqs():
   #initialise logger
   logger = logging.getLogger('treqs-on-git.treqs-ng')
   logger.setLevel(logging.INFO) 
   console_handler = logging.StreamHandler()
   console_handler.setLevel(logging.INFO) 
   #only display message for now.
   formatter = logging.Formatter('%(message)s')
   console_handler.setFormatter(formatter)
   logger.addHandler(console_handler)
   pass

@click.command(help='List treqs elements in this folder')
@click.option('--type', help='Limit action to specified treqs element type')
@click.option('--uid', help='Limit action to treqs element with specified id')
@click.option('--outlinks/--no-outlinks', default=False, help='Print outgoing tracelinks', show_default=True)
@click.option('--inlinks/--no-inlinks', default=False, help='Print incoming tracelinks', show_default=True)
@click.option('--recursive', type=bool, default=True, help='List treqs elements recursively in all subfolders.')
@click.argument('filename', default='.')#, help='Give a file or directory to list from.')
def list(type, filename, recursive, uid, outlinks, inlinks):
    le = list_elements()
    click.echo(le.list_elements(type,filename,recursive,uid,outlinks,inlinks))


@click.command(help='Creates a treqs element and prints it on the command line.')
@click.option('--tet', prompt='Which type should the element have?', default='requirement', help='The treqs element type that the new element should have.')
@click.option('--label', prompt='Which label should the element have?', default='my requirement', help='A short (less than one line) text describing this treqs element. Markdown (headings) encouraged.')
def create(tet,label):
   click.echo(create_elements.create_markdown_element(tet,label))

@click.command(help='Creates a link to a treqs element.')
@click.option('--linktype', prompt='Which type should the link have?', default='relatesto', help='The treqs link type, specifying the type of relationship to target .')
@click.option('--target', prompt='What UID does the target treqs element have?', default='UID missing', help='Use treqs list to find the right UID.')
def createlink(linktype, target):
   click.echo(create_elements.create_link(linktype, target))

@click.command(help='Checks for consistency of treqs elements.')
@click.option('--recursive', type=bool, default=True ,help='List treqs elements recursively in all subfolders.')
@click.option('--ttim', default='./ttim.json', help='Path to a type and traceability information model (TTIM) in json format.')
@click.argument('filename', default='.')#, help='Give a file or directory to list from.')
def check(recursive, filename, ttim):
   ce = check_elements()
   click.echo(ce.check_elements(filename,recursive, ttim))

@click.command(help='Process a treqs-controlled file, i.e. generate content in protected areas.')
@click.option('--recursive',type=bool,default=True,help='Process all subfolder recursively.',show_default=True)
@click.option('--plantuml/--no-plantuml',default=True,help='Toggle whether plantuml blocks should be processed.',show_default=True)
@click.argument('filename')
def process(filename,plantuml,recursive):
   # click.echo('processing (I mean: adding/replacing generated content in files...')
    pe = process_elements()
    if plantuml:
        pe.process_plantuml(filename, recursive)

treqs.add_command(list)
treqs.add_command(create)
treqs.add_command(createlink)
treqs.add_command(check)
treqs.add_command(process)

if __name__ == '__main__':
    treqs()
