import logging
from typing import Dict

class treqs_element:

    def __init__(self,element):
        self.uid = element.get("id")
        self.text = element.text
        self.label = self.extract_label(element.text)
        self.treqs_type = element.get("type")
        self.outlinks = []
        self.inlinks = []
        for treqslink in element.iter('treqs-link'):
            tl = treqs_link(self.uid, treqslink)
            self.outlinks.append(tl)

    def extract_label(self, text):
        # It is our convention to use the first none-empty line as label
        ret = "None"
        for line in text.splitlines():
            if line != "" and ret == "None":
                ret = line

        return ret

class treqs_link:
    def __init__(self, source, treqslinkelement):
        self.source = source # expect treqs_element's uid 
        self.target = treqslinkelement.get('target') # extract from text
        self.tlt = treqslinkelement.get('type') # extract tracelink type from text


class treqs_element_factory():

    _treqs_elements: Dict[str, treqs_element] =  {}

    def __init__(self) -> None:
        self.logger = logging.getLogger('treqs-on-git.treqs-ng')
        self.logger.info('treqs_element_fatory created')


    def get_treqs_element(self, element) -> treqs_element:
        # Let's cache treqs elements for later use.
        key = element.get("id")

        if not self._treqs_elements.get(key):
            self._treqs_elements[key] = treqs_element(element)

        return self._treqs_elements[key]

    def process_inlinks(self): 
        for te in self._treqs_elements.values():
            te.inlinks.clear()

        for te in self._treqs_elements.values():
            for tl in te.outlinks:
                target_te = self._treqs_elements.get(tl.target)
                if (target_te != None):
                    target_te.inlinks.append(tl)
