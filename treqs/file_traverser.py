import os
import xml.etree.cElementTree as ET
import logging

class file_traverser:
    def __init__(self):
        self.logger = logging.getLogger('treqs-on-git.treqs-ng')
        self.logger.info('file_traverser created')

    # Iterates over all files in a given directory (potentially recursive).
    # For each file, the provided handler function is called
    def traverse_file_hierarchy(self, file_name, recursive, handler, traversal_strategy = ""):
        success = 0
        # A specific traversal strategy function can be provided.
        # If none is provided, call the generic one (calling the handler for each file with a file name attribute)
        if traversal_strategy == "":
            self.logger.info("Choosing generic traversal strategy", file_name)
            traversal_strategy = self.traverse_generic_file

        if os.path.isfile(file_name):
            success = traversal_strategy(file_name, handler)
        else:
            if recursive:
                for root, directories, filenames in os.walk(file_name):
                    # for directory in directories
                    for filename in filenames:
                        if traversal_strategy(root + "/" + filename, handler) != 0:
                            success = 1
            else:
                listOfFiles = os.listdir(dir)
                for entry in listOfFiles:
                    if traversal_strategy(root + "/" + entry, handler) != 0:
                        success = 1
        
        return success

    # Traversal strategy for XML files. Iterates over all elements in the root and calls the handler funct with file name and the current element.
    def traverse_XML_file(self, file_name, handler):
        success = 0
        self.logger.info("Calling XML traversal with filename %s", file_name)
        try:
            self.logger.info('\n\n ### Processing elements in File %s', file_name)
            tree = ET.ElementTree(file=file_name)
            # self.logger.info(tree.getroot())
            root = tree.getroot()
            # self.logger.info('tag=%s, attrib=%s', root.tag, root.attrib)

            for element in root:
                # self.logger.info('| %s | %s | %s |', element.get("type"), self.extract_label(element.text), element.get("id"))
                if handler(file_name, element) != 0:
                    success = 1
        #Currently just ignore parse errors - we consider this a success for now
        except ET.ParseError:
            self.logger.info('\n\n ### Skipping elements in File %s due to parser error', file_name)
            return 0

        return success
    
    # Generic traversal strategy. Just calls the handler with the file name attribute.
    # The handler function decides what to do with the file
    def traverse_generic_file(self, file_name, handler):
        self.logger.info('\n\n ### Processing elements in File %s', file_name)
        return handler(file_name)
